package com.example.bestpractices.modules.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.bestpractices.core.data.ArticleList
import com.example.bestpractices.core.data.ArticleListElement
import com.example.bestpractices.core.data.DataState
import com.example.bestpractices.modules.home.repository.HomeRepository
import com.example.bestpractices.modules.home.viewmodel.HomeViewModel
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var dataListObserver: Observer<DataState<ArticleList>>

    private lateinit var viewModel: HomeViewModel

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `when viewModel getDataList success then sets liveData`() {
        val resultSuccess = MockRepository(mockArticleList)
        viewModel = HomeViewModel(repository = resultSuccess)
        viewModel.dataList.observeForever(dataListObserver)

        viewModel.getDataList()

        verify(dataListObserver).onChanged(DataState.Success(mockArticleList))
    }

    private val mockArticleList: ArticleList = arrayListOf(
        ArticleListElement(
            id = 1L,
            title = "Titulo teste",
            summary = "Descrição teste"
        )
    )
}

class MockRepository(private val articleList: ArticleList): HomeRepository {
    override fun getDataList(): Flow<DataState<ArticleList>> = flow {
        emit(DataState.Success(articleList))
    }
}