package com.example.bestpractices.modules.login.repository

import com.example.bestpractices.core.data.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.delay

class LoginRepository {

    fun makeLogin(): Flow<DataState<Boolean>> = flow {
        emit(DataState.Loading)

        delay(timeMillis = 3_000)
        emit(DataState.Success(true))
    }
}