package com.example.bestpractices.modules.home.repository

import com.example.bestpractices.core.data.ArticleList
import com.example.bestpractices.core.data.DataState
import com.example.bestpractices.core.network.ApiInterface
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class HomeRepositoryImpl
constructor(
    private val apiService: ApiInterface
): HomeRepository{

    override fun getDataList(): Flow<DataState<ArticleList>> = flow {
        emit(DataState.Loading)

        try {
            val result = apiService.getAllArticles()

            emit(DataState.Success(result))
        } catch (e: Exception){
            emit(DataState.Error(e))
        }
    }
}