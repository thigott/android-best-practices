package com.example.bestpractices.modules.login.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bestpractices.core.data.DataState
import com.example.bestpractices.modules.login.repository.LoginRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class LoginViewModel
constructor(
    private val loginRepository: LoginRepository
): ViewModel() {

    val loginState: MutableLiveData<DataState<Boolean>> = MutableLiveData()

    fun makeLogin(){
        viewModelScope.launch {
            loginRepository.makeLogin().collect { dataState ->
                loginState.value = dataState
            }
        }
    }
}