package com.example.bestpractices.modules.login.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.bestpractices.R
import com.example.bestpractices.core.data.DataState
import com.example.bestpractices.core.validators.EmailValidator
import com.example.bestpractices.databinding.FragmentLoginBinding
import com.example.bestpractices.modules.login.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {

    private val viewModel: LoginViewModel by viewModel()

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscribeObservers()
        setListeners()
        setInputSettings()
    }

    private fun subscribeObservers() {
        viewModel.loginState.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Loading -> binding.btLogin.setLoading()

                is DataState.Success -> this.findNavController().navigate(R.id.action_loginFragment_to_homeFragment)

                else -> binding.btLogin.setNormal()
            }
        })
    }

    private fun setListeners() {
        binding.btLogin.setOnClickListener {
            viewModel.makeLogin()
        }
    }

    private fun setInputSettings(){
        binding.run {
            inputEmail.initInputSettings(
                inputValidator = EmailValidator(),
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

        viewModel.loginState.value = null
    }
}