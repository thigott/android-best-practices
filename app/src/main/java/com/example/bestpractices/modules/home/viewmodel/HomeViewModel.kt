package com.example.bestpractices.modules.home.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bestpractices.core.data.ArticleList
import com.example.bestpractices.core.data.DataState
import com.example.bestpractices.modules.home.repository.HomeRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HomeViewModel
constructor(
    private val repository: HomeRepository
): ViewModel() {

    val dataList: MutableLiveData<DataState<ArticleList>> = MutableLiveData()

    fun getDataList(){
        viewModelScope.launch {
            repository.getDataList().collect { dataState ->
                dataList.value = dataState
            }
        }
    }
}