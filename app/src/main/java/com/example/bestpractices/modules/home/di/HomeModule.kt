package com.example.bestpractices.modules.home.di

import com.example.bestpractices.modules.home.repository.HomeRepository
import com.example.bestpractices.modules.home.repository.HomeRepositoryImpl
import com.example.bestpractices.modules.home.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val homeModule = module {
    factory<HomeRepository> {
        HomeRepositoryImpl(get())
    }
    
    viewModel {
        HomeViewModel(
            repository = get()
        )
    }
}