package com.example.bestpractices.modules.home.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.bestpractices.core.data.ArticleList
import com.example.bestpractices.core.data.DataState
import com.example.bestpractices.databinding.FragmentHomeBinding
import com.example.bestpractices.modules.home.adapter.ArticlesAdapter
import com.example.bestpractices.modules.home.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private val viewModel: HomeViewModel by viewModel()

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var articleAdapter: ArticlesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        subscribeObservers()
        getDataList()
    }

    private fun getDataList() = viewModel.getDataList()

    private fun initRecyclerView() {
        articleAdapter = ArticlesAdapter()
        binding.articleList.adapter = articleAdapter
    }

    private fun setDataToRecyclerView(articleList: ArticleList) = articleAdapter.submitList(articleList)

    private fun subscribeObservers(){
        viewModel.dataList.observe(viewLifecycleOwner, { dataState ->
            when (dataState) {
                is DataState.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.fragmentTitle.visibility = View.GONE
                    binding.articleList.visibility = View.GONE
                }

                is DataState.Success -> {
                    binding.progressBar.visibility = View.GONE
                    binding.fragmentTitle.visibility = View.GONE
                    binding.articleList.visibility = View.VISIBLE

                    setDataToRecyclerView(articleList = dataState.data)
                }

                else -> {
                    binding.progressBar.visibility = View.GONE
                    binding.fragmentTitle.visibility = View.VISIBLE
                    binding.articleList.visibility = View.GONE
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}