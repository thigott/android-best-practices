package com.example.bestpractices.modules.login.di

import com.example.bestpractices.modules.login.repository.LoginRepository
import com.example.bestpractices.modules.login.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginModule = module {
    factory {
        LoginRepository()
    }

    viewModel {
        LoginViewModel(loginRepository = get())
    }
}