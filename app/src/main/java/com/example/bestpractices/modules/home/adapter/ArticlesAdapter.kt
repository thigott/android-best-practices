package com.example.bestpractices.modules.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.bestpractices.core.data.ArticleListElement
import com.example.bestpractices.databinding.ArticleItemBinding

class ArticlesAdapter : ListAdapter<ArticleListElement, ArticlesAdapter.ArticlesViewHolder>(
    DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticlesViewHolder {
        return ArticlesViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: ArticlesViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ArticleListElement>() {
            override fun areItemsTheSame(
                oldItem: ArticleListElement,
                newItem: ArticleListElement
            ): Boolean = oldItem.id ?: 0L == newItem.id ?: 0L

            override fun areContentsTheSame(
                oldItem: ArticleListElement,
                newItem: ArticleListElement
            ): Boolean = oldItem == newItem

        }
    }

    class ArticlesViewHolder(
        private val itemBinding: ArticleItemBinding
    ): RecyclerView.ViewHolder(itemBinding.root){

        fun bind(article: ArticleListElement) {
            itemBinding.run {
                itemTitle.text = article.title ?: "Título"
                itemDescription.text = article.summary ?: "Descrição"
            }
        }

        companion object {
            fun create(parent: ViewGroup): ArticlesViewHolder {
                val itemBinding = ArticleItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return ArticlesViewHolder(itemBinding)
            }
        }
    }
}