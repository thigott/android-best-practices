package com.example.bestpractices.modules.home.repository

import com.example.bestpractices.core.data.ArticleList
import com.example.bestpractices.core.data.DataState
import kotlinx.coroutines.flow.Flow

interface HomeRepository {

    fun getDataList(): Flow<DataState<ArticleList>>
}