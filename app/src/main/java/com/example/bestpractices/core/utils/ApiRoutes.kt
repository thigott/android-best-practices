package com.example.bestpractices.core.utils

object ApiRoutes {

    const val baseUrl = "https://api.spaceflightnewsapi.net/v3/"

    const val allArticles = "articles"
}