package com.example.bestpractices.core.network

import com.example.bestpractices.core.data.ArticleList
import com.example.bestpractices.core.utils.ApiRoutes
import retrofit2.http.GET

interface ApiInterface {

    @GET(ApiRoutes.allArticles)
    suspend fun getAllArticles(): ArticleList
}