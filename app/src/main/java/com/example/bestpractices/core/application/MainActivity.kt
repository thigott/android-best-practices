package com.example.bestpractices.core.application

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bestpractices.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}