package com.example.bestpractices.core.customviews.input

import android.content.Context
import android.content.res.TypedArray
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doOnTextChanged
import com.example.bestpractices.R
import com.example.bestpractices.core.validators.Validator
import com.example.bestpractices.databinding.UserInputBinding

class UserInput @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var label: String? = null
    private var errorLabel: String? = null

    private val binding = UserInputBinding.inflate(LayoutInflater.from(context), this, true)

    private var validator: Validator? = null

    private var state: UserInputState = UserInputState.Normal
        set (value) {
            field = value
            refreshState()
        }

    init {
        setLayout(attrs)
        refreshState()
    }

    private fun setLayout(attrs: AttributeSet?) {
        attrs?.let { attributeSet ->
            val attributes = context.obtainStyledAttributes(attributeSet, R.styleable.UserInput)

            getAttributes(attributes)
            verifyAndSetPasswordInputType()

            attributes.recycle()
        }
    }

    private fun getAttributes(attributes: TypedArray){
        val titleResId = attributes.getResourceId(R.styleable.UserInput_input_label, 0)
        if (titleResId != 0) {
            label = context.getString(titleResId)
        }
    }

    private fun verifyAndSetPasswordInputType(){
        if (label == context.getString(R.string.password)) {
            binding.userInput.transformationMethod = PasswordTransformationMethod.getInstance()
        }
    }

    private fun refreshState(){
        binding.userInputLabel.text = label

        if (errorLabel != null)
            determineAndShowError()

        when (state) {
            is UserInputState.Normal -> binding.inputLayout.setBackgroundResource(R.drawable.user_input_bg_normal)
            is UserInputState.Error -> binding.inputLayout.setBackgroundResource(R.drawable.user_input_bg_error)
        }
    }

    private fun determineAndShowError(){
        binding.userErrorLabel.run {
            visibility = state.errorLabelVisibility
            text = errorLabel
        }
    }

    fun initInputSettings(
        inputValidator: Validator? = null,
        errorMessage: String? = null,
    ){
        if (inputValidator == null)
            return

        errorMessage?.let { errorLabel = it }

        validator = inputValidator

        binding.userInput.doOnTextChanged { text, _, _, _ ->
            state = if (validator?.isValid(text.toString()) != false)
                UserInputState.Normal
            else
                UserInputState.Error
        }
    }

    sealed class UserInputState(val errorLabelVisibility: Int) {
        object Normal : UserInputState(errorLabelVisibility = View.GONE)
        object Error : UserInputState(errorLabelVisibility = View.VISIBLE)
    }
}