package com.example.bestpractices.core.application

import android.app.Application
import com.example.bestpractices.core.di.networkModule
import com.example.bestpractices.modules.home.di.homeModule
import com.example.bestpractices.modules.login.di.loginModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class Application: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@Application)
            modules(
                listOf(
                    homeModule,
                    networkModule,
                    loginModule,
                )
            )
        }
    }
}