package com.example.bestpractices.core.data

typealias ArticleList = ArrayList<ArticleListElement>

data class ArticleListElement (
    val id: Long? = null,
    val title: String? = null,
    val url: String? = null,
    val imageURL: String? = null,
    val newsSite: String? = null,
    val summary: String? = null,
    val publishedAt: String? = null,
    val updatedAt: String? = null,
    val featured: Boolean? = null,
    val launches: List<Launch>? = null,
    val events: List<Any?>? = null
)

data class Launch (
    val id: String? = null,
    val provider: String? = null
)