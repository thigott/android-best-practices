package com.example.bestpractices.core.validators

class EmailValidator : Validator {

    override fun isValid(input: String?): Boolean {
        var response = false

        input?.let { value ->
            if (value.contains("@") && value.contains(".com"))
                response = true
        }

        return response
    }
}