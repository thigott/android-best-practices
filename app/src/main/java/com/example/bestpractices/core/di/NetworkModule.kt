package com.example.bestpractices.core.di

import com.example.bestpractices.core.network.ApiInterface
import com.example.bestpractices.core.utils.ApiRoutes
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    factory { provideHttpClient() }
    factory { provideApiInterface(get()) }
    single { provideRetrofit(get()) }
}

fun provideHttpClient(): OkHttpClient = OkHttpClient().newBuilder().build()

fun provideRetrofit(httpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(ApiRoutes.baseUrl).client(httpClient).
    addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideApiInterface(retrofit: Retrofit): ApiInterface = retrofit.create(ApiInterface::class.java)