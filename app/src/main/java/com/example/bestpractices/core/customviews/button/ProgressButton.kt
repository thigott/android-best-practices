package com.example.bestpractices.core.customviews.button

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.bestpractices.R
import com.example.bestpractices.databinding.ProgressButtonBinding

class ProgressButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var title: String? = null
    private var loadingTitle: String? = null

    private val binding = ProgressButtonBinding.inflate(LayoutInflater.from(context), this, true)

    private var state: ProgressButtonState = ProgressButtonState.Normal
        set(value) {
            field = value
            refreshState()
        }

    init {
        setLayout(attrs)
        refreshState()
    }

    private fun setLayout(attrs: AttributeSet?) {
        attrs?.let { attributeSet ->
            val attributes = context.obtainStyledAttributes(attributeSet, R.styleable.ProgressButton)
            setBackgroundResource(R.drawable.progress_button_background)

            getAttributes(attributes)

            attributes.recycle()
        }
    }

    private fun getAttributes(attributes: TypedArray){
        val titleResId = attributes.getResourceId(R.styleable.ProgressButton_progress_title, 0)
        if (titleResId != 0) {
            title = context.getString(titleResId)
        }

        val titleLoadingResId = attributes.getResourceId(R.styleable.ProgressButton_progress_loading_title, 0)
        if (titleLoadingResId != 0) {
            loadingTitle = context.getString(titleLoadingResId)
        }
    }

    private fun refreshState() {
        isEnabled = state.isEnabled
        isClickable = state.isEnabled
        refreshDrawableState()

        binding.progressButtonTitle.run {
            isEnabled = state.isEnabled
            isClickable = state.isEnabled
        }

        binding.progressBarLoading.visibility = state.progressVisibility

        when (state) {
            ProgressButtonState.Normal -> binding.progressButtonTitle.text = title
            ProgressButtonState.Loading -> binding.progressButtonTitle.text = loadingTitle
        }
    }

    fun setLoading() {
        state = ProgressButtonState.Loading
    }

    fun setNormal() {
        state = ProgressButtonState.Normal
    }

    sealed class ProgressButtonState(val isEnabled: Boolean, val progressVisibility: Int){
        object Normal : ProgressButtonState(true, View.GONE)
        object Loading : ProgressButtonState(false, View.VISIBLE)
    }
}