package com.example.bestpractices.core.validators

interface Validator {

    fun isValid(input: String?): Boolean
}